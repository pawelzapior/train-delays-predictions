# -*- coding: utf-8 -*-
"""
Created on Sun Jun 19 22:46:05 2022

@author: zapio
"""
import numpy as np
file = open('delays.csv', encoding="utf8")
data = np.genfromtxt(file, dtype = 'str', delimiter=',',skip_header=1)
np.random.shuffle(data)
n=10000
new_data=data[:n]



import pandas as pd 
pd.DataFrame(new_data).to_csv("delays_compressed.csv", header=False, index=False)
