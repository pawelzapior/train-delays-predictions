#%%

from sklearn.metrics import plot_confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn import tree
from sklearn.metrics import confusion_matrix
import numpy as np
import matplotlib.pyplot as plt

#%%
file = open('delays_compressed.csv', encoding="utf8")
data = np.genfromtxt(file, dtype = 'str', delimiter=',',skip_header=1)
#%%
#usunięcie niepotrzebnej kolumny
data=np.hstack([data[:,1:3],data[:,4:]])
#Wybieramy n losowych rekordów z bazy
n=10000
np.random.shuffle(data)
np.array(data)
print(data[0])

new_data=data[:n]
print("Wymiary tabeli: ")
print(new_data.shape)
# %%
#Usunięcie jednostki
unit='min'
    
new_data[:,4] = [int(sub.replace(unit, "").strip()) for sub in new_data[:,4]]    
print(new_data)
# %%
#Wykrylismy ze istnieja nienumeryczne dane w kolumnie z czasem odjazdu w postaci tekstu "Nie dotyczy". Usuwamy je."

for i in range(new_data.shape[0]):
    try:
        if new_data[i][3]=="Nie dotyczy":
            new_data=np.delete(new_data,i,0)
    except:
        break

# %%

#Funkcja kategoryzująca godzinę
def hour_cat(x):
    #HH:MM
    h=int(x[:2])
    return h
    
new_data[:,3] = [hour_cat(sub) for sub in new_data[:,3]]
    

#%%
#Dane do "regresjoklasyfikacji" 
regression_data = np.hstack([new_data[:,:4],new_data[:,5].reshape(-1,1), new_data[:,4].reshape(-1,1)])

X =regression_data[:,:-1]
Y =regression_data[:,-1]

X_train,X_test,Y_train,Y_test = train_test_split(X, Y, test_size=0.3)

#%%
from sklearn.preprocessing import OneHotEncoder

#Dane do regresji 

encoder = OneHotEncoder(sparse=False)
All_data_encoded = encoder.fit_transform(X,Y)

encoded_X_Train = encoder.transform(X_train)
encoded_X_Test = encoder.transform(X_test)


# %%
#drzewa decyzyjne

tree_classifier=tree.DecisionTreeClassifier(random_state=0)
tree_classifier.fit(encoded_X_Train,Y_train)
# %%
importance=tree_classifier.feature_importances_
print(importance)
print(tree_classifier.get_depth())
# %%
train_score=tree_classifier.score(encoded_X_Train,Y_train)
test_score=tree_classifier.score(encoded_X_Test,Y_test)
test_pred = tree_classifier.predict(encoded_X_Test)
print("Wyniki drzew decyzyjnych: ")
print(f'train score: {train_score}\ntest score: {test_score}')

#%%

import itertools

def plot_confusion_matrix(cm, target_names=None, cmap=None, normalize=True, labels=False, title='Confusion matrix'):
    accuracy = np.trace(cm) / float(np.sum(cm))
    misclass = 1 - accuracy

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        
    plt.figure(figsize=(8, 6))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()

    thresh = cm.max() / 1.5 if normalize else cm.max() / 2
    
    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names)
        plt.yticks(tick_marks, target_names)
    
    if labels:
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            if normalize:
                plt.text(j, i, "{:0.2f}".format(cm[i, j]),
                         horizontalalignment="center",
                         color="white" if cm[i, j] > thresh else "black")
            else:
                plt.text(j, i, "{:,}".format(cm[i, j]),
                         horizontalalignment="center",
                         color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label\naccuracy={:0.4f}; misclass={:0.4f}'.format(accuracy, misclass))
    plt.show()


# %%

conf_mat = confusion_matrix(Y_test, test_pred)
plot_confusion_matrix(conf_mat,title="Macierz pomyłek dla drzew decyzyjnych")

# %%
classifiers, scores_train, scores_test=[],[],[]
depths=np.arange(2,30)

for depht in depths:
    classifier=tree.DecisionTreeClassifier(random_state=0, max_depth=depht)
    classifier.fit(encoded_X_Train,Y_train)
    classifiers.append(classifier)
    scores_train.append(classifier.score(encoded_X_Train,Y_train))
    scores_test.append(classifier.score(encoded_X_Test,Y_test))
# %%
plt.plot(depths,scores_train,c='b',label='train')
plt.plot(depths,scores_test,c='r',label='test')
plt.legend()
plt.show()
print(2+np.argmax(scores_test))

# %%
#bayes
from sklearn.naive_bayes import GaussianNB

#%%
naive_model=naive_model=GaussianNB().fit(encoded_X_Train,Y_train)

train_sc=naive_model.score(encoded_X_Train,Y_train)
test_sc=naive_model.score(encoded_X_Test,Y_test)

naive_model_pred=naive_model.predict(encoded_X_Test)
print("Wyniki dla nainwego modelu Bayesa: ")
print(f'Train acc: {train_sc}\n Test acc: {test_sc}')


#%%

conf_mat = confusion_matrix(Y_test, naive_model_pred)
plot_confusion_matrix(conf_mat,title="Macierz pomyłek dla naiwnego modelu Bayesa")


# %%
#SVM
from sklearn import svm
from sklearn.model_selection import RandomizedSearchCV

modelSVM = svm.SVC()
#Buduje słownik hiperparametrów dla którego będę trenował dobór
parameters = {'kernel':['rbf'], 'C':[10.0], 'gamma': [0.01]}

findRSCV = RandomizedSearchCV(modelSVM, parameters, cv=3, random_state=5)
#trenuję model
findRSCV.fit(encoded_X_Train, Y_train)
SVM_pred=findRSCV.predict(encoded_X_Test)

#Wypisuje wyniki
print("\n \n Wyniki dla metody klasyfikacji SVM: ")
print("Najlepsze parametry:")
print(findRSCV.best_params_)
print("Średni wynik cross walidacji: ")
print(findRSCV.best_score_)
print("Dokładnosc na zbiorze testowym: ")
print(findRSCV.score(encoded_X_Test, Y_test))

#%%
conf_mat = confusion_matrix(Y_test, SVM_pred)
plot_confusion_matrix(conf_mat,title="Macierz pomyłek dla metody SVM")


#%%
'''
Jako zespół analizy danych spółki PKP otrzymaliśmy następującego maila:

Szanowni państwo,
 W z wojną na Ukrainie podjęliśmy decyzje o przekierowaniu pociągu 2601/0 (57) HETMAN, który do tej pory kursował w relacji Lublin Główny - Wrocław Główny do stacji Przemyśl Główny. 
	Zmienią się też godziny planowych przyjazdów pociągu – do Gliwic dojedzie on o 21:20, a do Rzeszowa Głównego o 23:56. 
Proszę o przeprowadzenie predykcji jakich opóźnień możemy spodziewać się na stacjach Gliwice i Rzeszów Główny Główny.

Z poważaniem, 
Zarząd PKP Intercity

'''
trasy = np.array([["2601/0 (57) HETMAN","PKP Intercity","Przemyśl Główny - Wrocław Główny",21, "Gliwice"],["2601/0 (57) HETMAN","PKP Intercity","Przemyśl Główny - Wrocław Główny",2, "Rzeszów Główny"]])

trasy_encoded=encoder.transform(trasy)

print("SVM: ",findRSCV.predict(trasy_encoded))
print("Bayes: ",naive_model.predict(trasy_encoded))
print("Drzewa decyzyjne: ",tree_classifier.predict(trasy_encoded))
'''
2601/0 (57) HETMAN
PKP Intercity
Relacja:
Lublin Główny - Wrocław Główny
Nowa relacja: 
Przemyśl Główny - Wrocław Główny
'''

