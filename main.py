#%%
from hashlib import new
from sklearn.metrics import plot_confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import plot_confusion_matrix
from sklearn.datasets import load_breast_cancer
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score, recall_score
from sklearn.metrics import classification_report
from sklearn.linear_model import LogisticRegression
import numpy as np
import matplotlib.pyplot as plt

#%%
file = open('delays_compressed.csv', encoding="utf8")
data = np.genfromtxt(file, dtype = 'str', delimiter=',',skip_header=1)
#%%
#usunięcie niepotrzebnej kolumny
data=np.hstack([data[:,1:3],data[:,4:]])
#Wybieramy n losowych rekordów z bazy
n=10000
np.random.shuffle(data)
np.array(data)
print(data[0])

new_data=data[:n]
# %%
#Usunięcie jednostki
unit='min'
    
new_data[:,4] = [int(sub.replace(unit, "").strip()) for sub in new_data[:,4]]    
# %%
#Wykrylismy ze istnieja nienumeryczne dane w kolumnie z czasem odjazdu w postaci tekstu "Nie dotyczy". Usuwamy je."

for i in range(new_data.shape[0]):
    try:
        if new_data[i][3]=="Nie dotyczy":
            new_data=np.delete(new_data,i,0)
    except:
        break
#%%     
    
y_delays=[]
for i in range(new_data.shape[0]):
    if int(new_data[i][4])>0:
        y_delays.append(True)
    else:
        y_delays.append(False)

# %%

#Funkcja kategoryzująca godzinę
def hour_cat(x):
    #HH:MM
    h=int(x[:2])
    return h
    
new_data[:,3] = [hour_cat(sub) for sub in new_data[:,3]]
    

#%%
#Dane do klasyfikacji
classification_data = np.hstack([new_data[:,:4],new_data[:,5].reshape(-1,1), np.array(y_delays).reshape(-1,1)])
print(classification_data[3,:])

X_class =classification_data[:,:-1]
Y_class =classification_data[:,-1]

X_train_clas,X_test_clas,Y_train_clas,Y_test_clas = train_test_split(X_class, Y_class, test_size=0.3)

#%%
#Dane do regresji 
regression_data = np.hstack([new_data[:,:4],new_data[:,5].reshape(-1,1), new_data[:,4].reshape(-1,1)])
print(regression_data[3,:])

X_reg =regression_data[:,:-1]
Y_reg =regression_data[:,-1]

X_train_reg,X_test_reg,Y_train_reg,Y_test_reg = train_test_split(X_reg, Y_reg, test_size=0.3)

#%%
from sklearn.preprocessing import OneHotEncoder

encoder_class = OneHotEncoder(sparse=False)
All_class_data_encoded = encoder_class.fit_transform(X_class, Y_class)

encoded_X_Train_class = encoder_class.transform(X_train_clas)
encoded_X_Test_class = encoder_class.transform(X_test_clas)
#Dane do regresji 

encoder_reg = OneHotEncoder(sparse=False)
All_reg_data_encoded = encoder_reg.fit_transform(X_reg,Y_reg)

encoded_X_Train_reg = encoder_reg.transform(X_train_reg)
encoded_X_Test_reg = encoder_reg.transform(X_test_reg)



# %%
#drzewa decyzyjne

tree_classifier=tree.DecisionTreeClassifier(random_state=0)
tree_classifier.fit(encoded_X_Train_class,Y_train_clas)
# %%
importance=tree_classifier.feature_importances_
print(importance)
print(tree_classifier.get_depth())
# %%
train_score=tree_classifier.score(encoded_X_Train_class,Y_train_clas)
test_score=tree_classifier.score(encoded_X_Test_class,Y_test_clas)
print(f'train score: {train_score}\ntest score: {test_score}')
# %%
#plot=plot_confusion_matrix(tree_classifier,X_test_clas,Y_test_clas,normalize=None,display_labels=[0,1],cmap=plt.cm.Blues)

plot=plot_confusion_matrix(tree_classifier, encoded_X_Test_class, Y_test_clas, values_format='.3f', normalize='true', cmap=plt.cm.Blues)
plt.title("Macierz pomyłek dla drzew decyzyjnych")
plt.show()
# %%
classifiers, scores_train, scores_test=[],[],[]
depths=np.arange(2,30)

for depht in depths:
    classifier=tree.DecisionTreeClassifier(random_state=0, max_depth=depht)
    classifier.fit(encoded_X_Train_class,Y_train_clas)
    classifiers.append(classifier)
    scores_train.append(classifier.score(encoded_X_Train_class,Y_train_clas))
    scores_test.append(classifier.score(encoded_X_Test_class,Y_test_clas))
# %%
plt.plot(depths,scores_train,c='b',label='train')
plt.plot(depths,scores_test,c='r',label='test')
plt.legend()
plt.show()
print(2+np.argmax(scores_test))

# %%
#bayes
from sklearn.naive_bayes import GaussianNB
from sklearn.manifold import TSNE
from sklearn.preprocessing import Normalizer
from sklearn.preprocessing import MinMaxScaler
#%%
naive_model=GaussianNB().fit(encoded_X_Train_class,Y_train_clas)

train_sc=naive_model.score(encoded_X_Train_class,Y_train_clas)
test_sc=naive_model.score(encoded_X_Test_class,Y_test_clas)

print(f'Train acc: {train_sc}\n Test acc: {test_sc}')

#%%

plot=plot_confusion_matrix(naive_model, encoded_X_Test_class, Y_test_clas, values_format='.3f', normalize='true', cmap=plt.cm.Blues)
plt.title("Macierz pomyłek dla naiwnego klasyfikatora Bayesa")
plt.show()
# %%
#SVM
from sklearn import svm
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import RandomizedSearchCV

modelSVM = svm.SVC()
#Buduje słownik hiperparametrów dla którego będę trenował dobór
parameters = {'kernel':['rbf'], 'C':[0.1, 1.0, 10.0 ,100.0], 'gamma': [0.001, 0.01, 0.1, 1, 10, 100]}

findRSCV = RandomizedSearchCV(modelSVM, parameters, cv=3, random_state=5)
#trenuję model
findRSCV.fit(encoded_X_Train_class, Y_train_clas)


#Wypisuje wyniki
print("\n \n Wyniki dla metody klasyfikacji SVM: ")
print("Najlepsze parametry:")
print(findRSCV.best_params_)
print("Średni wynik cross walidacji: ")
print(findRSCV.best_score_)
print("Dokładnosc na zbiorze testowym: ")
print(findRSCV.score(encoded_X_Test_class, Y_test_clas))

#%%
plot=plot_confusion_matrix(findRSCV, encoded_X_Test_class, Y_test_clas, values_format='.3f', normalize='true', cmap=plt.cm.Blues)
plt.title("Macierz pomyłek dla metody SVM")
plt.show()


#%%
#Regresja logistyczna
#Do regresji używamy: encoded_X_Test_reg, X_test_reg, encoded_X_Train_reg, X_train_reg
from sklearn.model_selection import GridSearchCV    

params = {"C":[0.01,0.1,1.0,10.0,100.0]}

classifier = GridSearchCV(LogisticRegression(max_iter = 1000), params, cv=4, verbose=1)

classifier.fit(encoded_X_Train_reg,Y_train_reg)
print()
print(classifier.best_estimator_)


#%%
preds = classifier.predict(encoded_X_Test_reg)

#%%



classifier = GridSearchCV(LogisticRegression(max_iter = 1000), params, cv=4, verbose=1)
classifier.fit(encoded_X_Train_reg,Y_train_reg)

preds_reg_log = classifier.predict(encoded_X_Test_reg)
print(classification_report(Y_test_reg,preds_reg_log))


#%%
#Regresja wielomianowa


from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error

def polynomial_regression(attributes, degree, values):

    regression = PolynomialFeatures(degree=degree)
    polynomial = regression.fit_transform(attributes)
    pol_regression = LinearRegression()
    pol_regression.fit(polynomial, values)
    return regression, pol_regression



def prepare_regression(X_train, y_train, X_test, y_test, max_degree):
    models, parameters, train_errors, test_errors, predictions = [], [], [], [], []
    
    # Wykonajmy regresje wielomianami stopnia 1, 2...
    for i in range(1, max_degree + 1):
        print("Próbuję wykonać regresję stopnia: ", i)
        poly_model, linear_model = polynomial_regression(X_train,
                                                         i,
                                                         y_train)
        models.append(linear_model)
        parameters.append([linear_model.intercept_, linear_model.coef_])
        # Dokonajmy predykcji na zbiorach treningowym i testowym i zmierzmy wartość błędu średniokwadratowego.
        y_train_prediction = linear_model.predict(poly_model.fit_transform(X_train))
        y_test_prediction = linear_model.predict(poly_model.fit_transform(X_test))
        predictions.append(y_test_prediction)
        train_errors.append(mean_squared_error(y_train_prediction, y_train))
        test_errors.append(mean_squared_error(y_test_prediction, y_test))


    return models, parameters, train_errors, test_errors, predictions


#Do regresji używamy: encoded_X_Test_reg, Y_test_reg, encoded_X_Train_reg, Y_train_reg

models, parameters, train_errors, test_errors, predictions = prepare_regression(encoded_X_Train_reg,
                                                                                Y_train_reg.reshape(-1,1),
                                                                                encoded_X_Test_reg,
                                                                                Y_test_reg.reshape(-1,1),
                                                                                2) 
#Testy wykazały że nie jesteśmy w stanie ze wzgledu na ograniczone zasoby zbudować model regresji wielomianowej o stopniu wyższym niż 2. Do modelu regresji o deg=3 potrzebne było 236 GB pamięci RAM dla zbioru 500 elementowego.
for i in range(1, 3):
    print(f'''Stopień wielomianu: {i},
              błąd na zb. treningowym: {train_errors[i - 1]},
              błąd na zb. testowym: {test_errors[i - 1]}''')
              
              
plt.plot(train_errors, color='blue', label='Błędy na zbiorze treningowym')
plt.plot(test_errors, color='red', label='Błędy na zbiorze testowym')
plt.legend()
plt.xticks(np.arange(len(train_errors)), np.arange(len(train_errors)) + 1)

plt.xlabel('Stopień wielomianu')
plt.ylabel('Wartość błędu')
plt.show()              



#%% 
'''
Jako zespół analizy danych spółki PKP otrzymaliśmy następującego maila:

Szanowni państwo,
 W z wojną na Ukrainie podjęliśmy decyzje o przekierowaniu pociągu 2601/0 (57) HETMAN, który do tej pory kursował w relacji Lublin Główny - Wrocław Główny do stacji Przemyśl Główny. 
	Zmienią się też godziny planowych przyjazdów pociągu – do Gliwic dojedzie on o 21:20, a do Rzeszowa Głównego o 23:56. 
Proszę o przeprowadzenie predykcji jakich opóźnień możemy spodziewać się na stacjach Gliwice i Rzeszów Główny Główny.

Z poważaniem, 
Zarząd PKP Intercity

'''
trasy = np.array([["2601/0 (57) HETMAN","PKP Intercity","Przemyśl Główny - Wrocław Główny",21, "Gliwice"],["2601/0 (57) HETMAN","PKP Intercity","Przemyśl Główny - Wrocław Główny",2, "Rzeszów Główny"]])

trasy_encoded=encoder_class.transform(trasy)

print("SVM: ",findRSCV.predict(trasy_encoded))
print("Bayes: ",naive_model.predict(trasy_encoded))
print("Drzewa decyzyjne: ",tree_classifier.predict(trasy_encoded))
'''
2601/0 (57) HETMAN
PKP Intercity
Relacja:
Lublin Główny - Wrocław Główny
Nowa relacja: 
Przemyśl Główny - Wrocław Główny
'''