![Train](./img/t%C5%82o.png)

# Train Delays Predictions 


## Project objectives
- Classification of whether the train will arrive on time based on the features of the relationship 
- Comparative analysis of results obtained by using decision tree methods, naive Bayes classifier and SVM classifier
- Application of logistic and polynomial regression to determine train delay time  
- Classification of train delay time using each of the aforementioned algorithms (secondary objective)
- Comparison of the effect of using regression and classification to determine train delay time (secondary objective)

## Dataset

The dataset contains information about delays on polish railroads published by the Polish State Railways in real time. Data was scraped from https://infopasazer.intercity.pl. It covers the period from the midnight of 2022-05-16 until the midnight of 2022-05-30. Observations were collected every 5 minutes.

Dataset source: https://www.kaggle.com/datasets/bartek358/train-delays   
Licence: CC0: Public Domain

## Requirements

Tested with Python 3.7

## Other remarks
Co-author: Zuzanna Stachura  
Podstawy Uczenia Maszynowego, Silesian University Of Technology 2022 